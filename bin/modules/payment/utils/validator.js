const joi = require('joi');
const validate = require('validate.js');
const Mongo = require('../../../helpers/databases/mongodb/db');
const wrapper = require('../../../helpers/utils/wrapper');
const config = require('../../../infra/configs/global_config');

const isValidPayload = (payload, constraint) => {
  const { value, error } = joi.validate(payload, constraint);
  if(!validate.isEmpty(error)){
    return wrapper.error('fail', error, 409);
  }
  return wrapper.data(value, 'success', 200);

};

const validateConstraints = async (values,constraints) => {
  if(validate(values,constraints)){
      return wrapper.error('Bad Request',validate(values,constraints),400);
  }else{
      return wrapper.data(true);
  }
}

const isValidParamPostOnePayment = async (payload) => {
  let constraints = {};
  let values = {};
  constraints[payload.id_users] = {length: {minimum: 1}};
  constraints[payload.product] = {length: {minimum: 1}};
  constraints[payload.total_value] = {length: {minimum: 1}};
  constraints[payload.status] = {length: {minimum: 1}};
  values[payload.id_users] = payload.id_users;
  values[payload.product] = payload.product;
  values[payload.total_value] = payload.total_value;
  values[payload.status] = payload.status;
  return await validateConstraints(values,constraints);
}

const isValidParamGetOnePayment = async (payload) => {
  let constraints = {};
  let values = {};
  constraints[payload.id_users] = {length: {minimum: 1}};
  constraints[payload.product] = {length: {minimum: 1}};
  constraints[payload.total_value] = {length: {minimum: 1}};
  constraints[payload.status] = {length: {minimum: 1}};
  values[payload.id_users] = payload.id_users;
  values[payload.product] = payload.product;
  values[payload.total_value] = payload.total_value;
  values[payload.status] = payload.status;
  return await validateConstraints(values,constraints);
}

const isValidParamGetManyPayments = async (payload) => {
  let constraints = {};
  let values = {};
  constraints[payload.id_users] = {length: {minimum: 1}};
  constraints[payload.product] = {length: {minimum: 1}};
  constraints[payload.total_value] = {length: {minimum: 1}};
  constraints[payload.status] = {length: {minimum: 1}};
  values[payload.id_users] = payload.id_users;
  values[payload.product] = payload.product;
  values[payload.total_value] = payload.total_value;
  values[payload.status] = payload.status;
  return await validateConstraints(values,constraints);
}

module.exports = {
  isValidParamPostOnePayment : isValidParamPostOnePayment,
  isValidParamGetOnePayment : isValidParamGetOnePayment,
  isValidParamGetManyPayments : isValidParamGetManyPayments,
};
