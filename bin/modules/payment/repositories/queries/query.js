
const Mongo = require('../../../../helpers/databases/mongodb/db');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const ObjectId = require('mongodb').ObjectId;

const findOnePayment = async (params) => {
  const db = new Mysql(config.get('/mysqlConfig'));
  const table =  'payment';
  const id = params.id
  const recordset = await db.findOne(table, id);
  return recordset;
}

const findAllPayments = async (parameter) => {
  const db = new Mysql(config.get('/mysqlConfig'));
  const table =  'payment';
  const recordset = await db.findMany(table); 
  return recordset;
}

module.exports = {
  findOnePayment : findOnePayment,
  findAllPayments : findAllPayments
}