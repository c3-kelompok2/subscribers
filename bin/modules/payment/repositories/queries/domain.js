
const query = require('./query');
const wrapper = require('../../../../helpers/utils/wrapper');
const { NotFoundError } = require('../../../../helpers/error');

class Payment {

  constructor(param){
    this.id = param.id,
    this.id_users = param.id_users,
    this.product = param.product,
    this.total_value =  param.total_value,
    this.status = param.status
  }

  async viewOnePayment(){
    const param = {"id": this.id};
    const result = await query.findOnePayment(param);

    if(result.err){
        return result;
    }else{
        return wrapper.data(result.data);
    }
  }

  async viewAllPayments(){
    const param = {};
    const result = await query.findAllPayments(param);

    if(result.err){
        return result;
    }else{
        return wrapper.data(result.data);
    }
  }

}

module.exports = Payment;
