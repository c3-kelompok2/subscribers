
const Payment = require('./domain');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const config = require('../../../../infra/configs/global_config');

const getOnePayment = async (queryParam) => {
  const getQuery = async (queryParam) => {
      const payment = new Payment(queryParam);
      const result = await payment.viewOnePayment();
      return result;
  }
  const result = await getQuery(queryParam);
  return result;
}

const getAllPayments = async (queryParam) => {
  const getQuery = async (queryParam) => {
      const payment = new Payment(queryParam);
      const result = await payment.viewAllPayments();
      return result;
  }
  const result = await getQuery(queryParam);
  return result;
}

module.exports = {
  getOnePayment : getOnePayment,
  getAllPayments : getAllPayments
};
