
const Payment = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');

const postOnePayment = async (payload) => {
  const payment = new Payment();
  const postCommand = async (payload) => {
      return await payment.addPayment(payload);
  }
  return postCommand(payload);
}

const updateOnePayment = async (id, payload) => {
  const payment = new Payment();
  const putCommand = async (id, payload) => {
      return await payment.updatePayment(id, payload);
  }
  return putCommand(id, payload);
}

const deleteOnePayment = async (id) => {
  const payment = new Payment();
  const delCommand = async (id) => {
      return await payment.deletePayment(id);
  }
  return delCommand(id);
}



module.exports = {
  postOnePayment : postOnePayment,
  updateOnePayment : updateOnePayment,
  deleteOnePayment : deleteOnePayment
};
