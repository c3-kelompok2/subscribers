
const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const jwtAuth = require('../../../../auth/jwt_auth_helper');
const commonUtil = require('../../../../helpers/utils/common');
const logger = require('../../../../helpers/utils/logger');
const model = require('./command_model')
const command = require('./command');
const validate = require('validate.js');
const { NotFoundError, UnauthorizedError, ConflictError } = require('../../../../helpers/error');

class Payment {

  async addPayment(payload){
    const data = [payload];
    let view = model.payments();
    view = data.reduce((accumulator, value) => {
        if(!validate.isEmpty(value.id_users)){accumulator.id_users = value.id_users;}
        if(!validate.isEmpty(value.product)){accumulator.product = value.product;}
        if(!validate.isEmpty(value.total_value)){accumulator.total_value = value.total_value;}
        if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
        if(!validate.isEmpty(value.status)){accumulator.status = value.status;}          
        return accumulator;
    }, view);
    const document = view;
    const result = await command.insertOne(document);
    return result;
}

async updatePayment(payload){
  const data = [payload];
  let view = model.payments();
  view = data.reduce((accumulator, value) => {
    if(!validate.isEmpty(value.id_users)){accumulator.id_users = value.id_users;}
    if(!validate.isEmpty(value.product)){accumulator.product = value.product;}
    if(!validate.isEmpty(value.total_value)){accumulator.total_value = value.total_value;}
    if(!validate.isEmpty(value.id)){accumulator.id = value.id;}
    if(!validate.isEmpty(value.status)){accumulator.status = value.status;}            
      return accumulator;
  }, view);
  const document = view;
  const result = await command.updateOne(document);
  return result;
}

async deletePayment(params){
  const result = await command.deleteOne(params);
  return result;
}

}

module.exports = Payment;
